
CREATE DATABASE IF NOT EXISTS TaniaRascia;

use TaniaRascia;

DROP TABLE IF EXISTS Users;

CREATE TABLE Users (
	Id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	Firstname VARCHAR(30) NOT NULL,
	Lastname VARCHAR(30) NOT NULL,
	Email VARCHAR(50) NOT NULL,
	Age INT(3),
	Location VARCHAR(50),
	-- om keywords te escapen, insluiten tussen backticks ``
	`Date` TIMESTAMP
);