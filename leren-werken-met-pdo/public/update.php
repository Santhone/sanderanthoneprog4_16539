<?php
    // alleen uit te voeren als er op de submit knop is gedrukt
        include('../config.php');
        include('../common.php');
        $statement = false;
        try {
            $sql = 'SELECT * FROM Users' ;
             // echo $sql;
            // return;
            $connection = new \PDO($host, $user, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $result = $statement->fetchAll();

        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }

    include('template/header.php');
?>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Email</th>
            <th>Leeftijd</th>
            <th>Plaats</th>
            <th>Date</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
<?php
        if ($result && $statement->rowCount() > 0) {
            foreach ($result as $row) {
?>
            <tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['Firstname'];?></td>
                <td><?php echo $row['Lastname'];?></td>
                <td><?php echo $row['Email'];?></td>
                <td><?php echo $row['Age'];?></td>
                <td><?php echo $row['Location'];?></td>
                <td><?php echo $row['Date'];?></td>
                <td><a href="update-single.php?Id=<?php echo $row['Id'];?>">Edit</a></td>
            </tr>        
<?php
            }
        }

?>
    </tbody>
</table>

<?php
    include('template/footer.php');
?>