<?php
// setcookie('voornaam', 'Mo', time() + 86400 * 2);
// setcookie('familienaam', 'Mo', time() + 86400 * 2);
// set the expiration date to one hour ago
setcookie("voornaam", "", time() - 3600);
setcookie("familienaamnaam", "", time() - 3600);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leren werken met cookies</title>
</head>
<body>
<pre>
    <?php
        var_dump($_COOKIE);
    ?>
</pre>
<form action="" method="post">
    <div><label for="voornaam">Voornaam</label><input type="text" name="voornaam" id="voornaam"></div>
    <div><label for="familienaam">Familienaam</label><input type="text" name="familienaam" id="familienaam"></div>
    <input type="submit" value="Send">
</form>
</body>
</html>