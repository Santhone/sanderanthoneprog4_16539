<?php
namespace ModernWays\MoeMaarTevreden;

class Setup {
    /**
     * initialize tables
     */
     public static function fillRestaurantsFromJSON() {
        $list = json_decode(file_get_contents(__DIR__ . '/../../../../data/restaurants.json'), true);
        $restaurantList = $list['Restaurant'];

        foreach ($restaurantList as $item) {
            //Remove the "Id" element, which has the index "Id".
            // Om te inserten hebben we geen Id nodig
            unset($item['Id']);    
            // print_r($item);
            \ModernWays\Dal::create('Restaurants', $item);
            echo \ModernWays\Dal::getMessage() . '<br />';
        }
     }
}
