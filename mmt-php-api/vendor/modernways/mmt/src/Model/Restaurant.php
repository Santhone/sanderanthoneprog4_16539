<?php
namespace ModernWays\FricFrac\Model;

class Restaurant {
    private $id;
    private $name;
    private $image;
    private $chef;
    private $sterren;
    private $land;
    private $provincie;
    private $gemeente;
    private $Longitude;
    private $Latitude;
    private $comment;
    
    public function setId($value) {
        $this->id = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setName($value) {
        $this->name = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setImage($value) {
        $this->image = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setChef($value) {
        $this->chef = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setSterren($value) {
        $this->sterren = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLand($value) {
        $this->land = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setProvincie($value) {
        $this->provincie = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setGemeente($value) {
        $this->gemeente = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLongitude($value) {
        $this->Longitude = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLatitude($value) {
        $this->Latitude = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setComment($value) {
        $this->comment = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getImage() {
        return $this->image;
    }
    
    public function getChef() {
        return $this->chef;
    }
    
    public function getSterren() {
        return $this->sterren;
    }
    
    public function getLand() {
        return $this->land;
    }
    
    public function getProvincie() {
        return $this->provincie;
    }
    
    public function getGemeente() {
        return $this->gemeente;
    }
    
    public function getLongitude() {
        return $this->Longitude;
    }
    
    public function getLatitude() {
        return $this->Latitude;
    }
    
    public function getComment() {
        return $this->comment;
    }
    
    public function toArray() {
        return array(
            "Id" => $this->getId(),
            "Name" => $this->getName(),
            "Image" => $this->getImage(),
            "Chef" => $this->getChef(),
            "Sterren" => $this->getSterren(),
            "Land" => $this->getLand(),
            "Provincie" => $this->getProvincie(),
            "Gemeente" => $this->getGemeente(),
            "Longitude" => $this->getLongitude(),
            "Latitude" => $this->getLatitude(),
            "Comment" => $this->getComment(),    
        );
    }
    
    public function arrayToObject($array) {
        $this->setId($array['Id']);
        $this->setName($array['Name']);
        $this->setImage($array['Image']);
        $this->setChef($array['Chef']);
        $this->setSterren($array['Sterren']);
        $this->setLand($array['Land']);
        $this->setProvincie($array['Provincie']);
        $this->setGemeente($array['Gemeente']);
        $this->setLongitude($array['Longitude']);
        $this->setLatitude($array['Latitude']);
        $this->setComment($array['Comment']);
    }
    
}