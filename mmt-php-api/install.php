<?php
include ('data/config.ini');

$db = parse_ini_file("data/config.ini");
$username = $db['username'];
$password = $db['password'];
$driver = $db['driver'];
$port = $db['port'];
$databaseName = $db['database'];
$host = "$driver:host=localhost:$port;dbname=$databaseName";
$options = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );
    
try {
    // in php gebruiken we backslashes om namespaces aan te geven.
    // Eén backslash wil zeggen dat de klasse in de rootnamespace staat.
    $connection = new \PDO($host, $username, $password, $options);
	$sql = file_get_contents('data/sqlscripts/mmt-ddl.sql');
	$connection->exec($sql);
	
	echo "Database and table Users created successfully.";
} catch(\PDOException $error) {
	echo $sql . "<br>" . $error->getMessage();
}