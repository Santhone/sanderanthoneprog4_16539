<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="TestOpdrachtenBeginnenMetPHP.css">
    
    <title>Welkom</title>
</head>
<body>
        <span>click the checkbox to hide or unhide the solution</span></br>
    <h1>ARRAY</h1>
        <input id="grouptrigger" type="checkbox">
        <div class="solutiongroup">
            <?php
            include 'Opdrachten/OpdrachtenBeginnenMetPHP.php';
            foreach($arraySolutions as $xyz): ?>
            <h2> <?php echo $xyz; ?> </h2>
            <input id="trigger" type="checkbox">
            <div class="solution">
                <?php echo $xyz(); ?>
            </div>
            <?php endforeach; ?>
        </div>
    <h1>FOR_LOOP</h1>
        <input id="grouptrigger" type="checkbox">
        <div class="solutiongroup">
            <?php
            
            foreach($for_loopSolutions as $abc): ?>
            <h2> <?php echo $abc; ?> </h2>
            <input id="trigger" type="checkbox">
            <div class="solution">
                <?php echo $abc(); ?>
            </div>
            <?php endforeach; ?>
        </div>
    <h1>FUNCTIONS</h1>
        <input id="grouptrigger" type="checkbox">
        <div class="solutiongroup">
            <?php
            
            foreach($functionSolutions as $pqr): ?>
            <h2> <?php echo $pqr; ?> </h2>
            <input id="trigger" type="checkbox">
            <div class="solution">
            <?php echo $pqr(); ?>
            </div>
            <?php endforeach; ?>
        </div>
</body>
</html>