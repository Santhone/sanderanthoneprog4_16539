<?php
    $eventCategoryList = \ModernWays\FricFrac\Dal\EventCategory::readAll();
?>
<aside>
    <table>
        <?php
            if ($eventCategoryList) {
                foreach($eventCategoryList as $eventCategoryItem) {
        ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?php echo $eventCategoryItem['Id'];?>">-></a></td>
                    <td><?php echo $eventCategoryItem['Name'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen event topics gevonden</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
