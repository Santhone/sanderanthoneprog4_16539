<?php
    $roleList = \ModernWays\FricFrac\Dal\Role::readAll();
?>
<aside>
    <table>
        <?php
            if ($roleList) {
                foreach($roleList as $roleItem) {
        ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?php echo $roleItem['Id'];?>">-></a></td>
                    <td><?php echo $roleItem['Name'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen rollen gevonden</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
