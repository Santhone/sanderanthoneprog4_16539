<?php
    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();
?>
<aside>
    <table>
        <?php
            if ($countryList) {
                foreach($countryList as $countryItem) {
        ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?php echo $countryItem['Id'];?>">-></a></td>
                    <td><?php echo $countryItem['Name'];?></td>
                    <td><?php echo $countryItem['Code'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen landen gevonden</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
