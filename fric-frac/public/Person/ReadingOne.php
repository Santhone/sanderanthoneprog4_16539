
<?php
    include ('../template/header.php');
    $countryList = \ModernWays\FricFrac\Dal\Country::readAll();

    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Person::readOneById($id));

    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Person::delete($id);
            header("Location: Index.php");
       }
    } 
?>
<main>
    <article>
        <header>
            <?php include ('titel.php'); ?>
        <nav>
            <a href="UpdatingOne.php?Id=<?php echo $model->getId();?>">Updating</a>
            <a href="InsertingOne.php">Inserting</a>
            <button type="submit" name="uc" value="delete" form="form">Delete</button>
           <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <div>
                <label for="FirstName">Voornaam</label>
                <input type="text" id="FirstName" name="FirstName" readonly value="<?php echo $model->getFirstName();?>"/>
            </div>
            <div>
                <label for="LastName">Achternaam</label>
                <input type="text" readonly id="LastName" name="LastName" value="<?php echo $model->getLastName();?>"/>
            </div>
            <div>
                <label for="Email">Email</label>
                <input type="email"  id="Email" name="Email" readonly value="<?php echo $model->getEmail();?>"/>
            </div>           
             <div>
                <label for="Address1">Adres 1</label>
                <input type="text"  id="Address1" name="Address1" readonly value="<?php echo $model->getAddress1();?>"/>
            </div>
             <div>
                <label for="Address2">Adres 2</label>
                <input type="text"  id="Address2" name="Address2" readonly value="<?php echo $model->getAddress2();?>"/>
            </div>
             <div>
                <label for="PostalCode">postcode</label>
                <input type="text"  id="PostalCode" name="PostalCode" readonly value="<?php echo $model->getPostalCode();?>"/>
            </div>
             <div>
                <label for="City">Plaats</label>
                <input type="text"  id="City" name="City" readonly value="<?php echo $model->getCity();?>"/>
            </div>
            <div>
                <label for="CountryId">Land</label>
                <select id="CountryId" name="CountryId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($countryList) {
                        foreach ($countryList as $row) {
                    ?>
                    <option value="<?php echo $row['Id'];?>" 
                        <?php echo $model->getCountryId() === $row['Id'] ? 'SELECTED' : '';?>>
                        <?php echo $row['Name'];?>
                    </option>
                    <?php
                        }
                    }
                    ?>                
                </select>
            </div>            
             <div>
                <label for="Phone1">Telefoon</label>
                <input type="text"  id="Phone1" name="Phone1" readonly value="<?php echo $model->getPhone1();?>"/>
            </div>
            <div>
                <label for="Birthday">Geboortedatum</label>
                <input type="date"  id="Birthday" name="Birthday" readonly value="<?php echo $model->getBirthday();?>"/>
            </div>           
            <div>
                <label for="Rating">Tevreden</label>
                 <input type="range" name="Rating" id="Rating" min="1" max="10" value="<?php echo $model->getRating();?>" />
            </div>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>






<?php include('../template.footer.php');?>