
<?php
    include ('../template/header.php');
    $CountryList = \ModernWays\FricFrac\Dal\Country::readAll();
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Person();

        $model->setFirstName($_POST['FirstName']);
        $model->setLastName($_POST['LastName']);
        $model->setEmail($_POST['Email']);
        $model->setAddress1($_POST['Address1']);
        $model->setAddress2($_POST['Address2']);
        $model->setPostalCode($_POST['PostalCode']);
        $model->setCity($_POST['City']);
        $model->setCountryId($_POST['CountryId']);
        $model->setPhone1($_POST['Phone1']);
        $model->setBirthday($_POST['Birthday']);
        $model->setRating($_POST['Rating']);
    
        \ModernWays\FricFrac\Dal\Person::create($model->toArray());
    }
?>
<main>
    <article>
        <header>
            <?php include('titel.php'); ?>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <div>
                <label for="FirstName">Voornaam</label>
                <input type="text" required id="FirstName" name="FirstName"/>
            </div>
             <div>
                <label for="LastName">Achternaam</label>
                <input type="text" id="LastName" name="LastName"/>
            </div>
             <div>
                <label for="Email">Email</label>
                <input type="email"  id="Email" name="Email"/>
            </div>           
            <div>
                <label for="Address1">Adres</label>
                <input type="text"  id="Address1" name="Address1"/>
            </div>           
             <div>
                <label for="Address2">Adres 2</label>
                <input type="text"  id="Address2" name="Address2"/>
            </div>           
            <div>
                <label for="PostalCode">Postcode</label>
                <input type="text"  id="PostalCode" name="PostalCode"/>
            </div>              
            <div>
                <label for="City">Stad/Dorp</label>
                <input type="text"  id="City" name="City"/>
            </div>     
    
             <div>
                <label for="Phone1">Telefoonnummer</label>
                <input type="text" required id="Phone1" name="Phone1"/>
            </div>                
            <div>
                <label for="Birthday">Verjaardag</label>
                <input type="date"  id="Birthday" name="Birthday"/>
            </div>
            <div>
                <label for="Rating">Tevredenheid </label>
                 <input type="range" name="Rating" id="Rating" min="1" max="100" value="50" />
            </div>          
            <div>
                <label for="CountryId">Land </label>
                <select id="CountryId" name="CountryId">
                    <!-- option elementen -->
                    <?php
                    if ($CountryList) {
                        foreach ($CountryList as $row) {
                    ?>
                    <option value="<?php echo $row['Id'];?>"><?php echo $row['Name'];?></option>
                    <?php
                        }
                    }
                    ?>                
                </select>
            </div>

                      
            </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>