<?php
    $eventTopicList = \ModernWays\FricFrac\Dal\EventTopic::readAll();
?>
<aside>
    <table>
        <?php
            if ($eventTopicList) {
                foreach($eventTopicList as $eventTopicItem) {
        ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?php echo $eventTopicItem['Id'];?>">-></a></td>
                    <td><?php echo $eventTopicItem['Name'];?></td>
                </tr>
        <?php
                }
            } else {
        ?>
                <tr><td>Geen event topics gevonden</td></tr>
        <?php
            } 
        ?>
    </table>
</aside>
