<?php
namespace ModernWays\FricFrac\Dal;

/**
* Met dank aan Ben, Agge, 
*
* 
*/

class Person extends Base {
    
    public static function delete($id) {
        $success = 0;
        if (self::connect()) {
            $Id = \ModernWays\Helpers::escape($id);
      
              try {
                $sql = 'DELETE FROM Person WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $Id);
                $statement->execute();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "De rij met id $id bestaat niet!";
                } else {
                    self::$message = "De rij met id $id is gedeleted!";
                    
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Fout: verwijderen mislukt!";
            }
         
        }
        return $success;
    }
    public static function create($post) {
        $success = false;
        if (self::connect()) {
            $newEntity = array(
                'FirstName' => \ModernWays\Helpers::escape($post['FirstName']),
                'LastName' => \ModernWays\Helpers::escape($post['LastName']),
                'Email' => \ModernWays\Helpers::escape($post['Email']),
                'Address1' => \ModernWays\Helpers::escape($post['Address1']),
                'Address2' => \ModernWays\Helpers::escape($post['Address2']),
                'PostalCode' => \ModernWays\Helpers::escape($post['PostalCode']),
                'City' => \ModernWays\Helpers::escape($post['City']),
                'CountryId' => \ModernWays\Helpers::escape($post['CountryId']),
                'Phone1' => \ModernWays\Helpers::escape($post['Phone1']),
                'Birthday' => \ModernWays\Helpers::escape($post['Birthday']),
                'Rating' => \ModernWays\Helpers::escape($post['Rating']),
      );
            try {
                $sql = sprintf("INSERT INTO %s (%s) VALUES (:%s)", 'Person', 
                    implode(', ', array_keys($newEntity)), 
                    implode(', :', array_keys($newEntity)));
                $statement = self::$connection->prepare($sql);
                $statement->execute($newEntity);
                self::$message = 'Rij is toegevoegd in Person!';
                $success = true;
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Rij is niet toegevoegd in Person!";
            }
        }
        return $success;
    }
    
    public static function readOneByFirstName($firstName) {
        $success = 0;
        if (self::connect()) {
            $FirstName = \ModernWays\Helpers::escape($firstName);
            try {
                $sql = 'SELECT * FROM Person WHERE FirstName = :FirstName';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':FirstName', $firstName, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetchAll();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "Geen persoon met de naam $name gevonden.";
                } else {
                    self::$message = "Alle Personen met de naam $name zijn gevonden.";
                }
            } catch (\PDOException $exception) {
               self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
             }
        } 
        return $success;
    }
    
    public static function readOneByLastName($lastName) {
        $success = 0;
        if (self::connect()) {
            $LastName = \ModernWays\Helpers::escape($lastName);
            try {
                $sql = 'SELECT * FROM Person WHERE LastName = :LastName';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':FirstName', $lastName, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetchAll();
                $success = $statement->rowCount();
                if ($success == 0) {
                    self::$message = "Geen persoon met de naam $name gevonden.";
                } else {
                    self::$message = "Alle Personen met de naam $name zijn gevonden.";
                }
            } catch (\PDOException $exception) {
               self::$message = "Syntax fout in SQL: {$exception->getMessage()}";
             }
        } 
        return $success;
    }
    
    public static function readOneById($id) {
        $success = 0;
        if (self::connect()) {
            $id = \ModernWays\Helpers::escape($id);
            try {
                $sql = 'SELECT * FROM Person WHERE Id = :Id';
                $statement = self::$connection->prepare($sql);
                $statement->bindParam(':Id', $id, \PDO::PARAM_STR);
                $statement->execute();
                $result = $statement->fetch(\PDO::FETCH_ASSOC);
                if ($result) {
                    self::$message = "De rij met de id $id is ingelezen.";
                } else {
                    self::$message = "De rij met de id $id is niet ingelezen.";
                }
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "Geen persoon met de id $id gevonden.";
            }
        } 
        return $result;
    }
    public static function readAll() {
        $result = null;
        if (self::connect()) {
            try {
                $sql = 'SELECT * FROM Person ORDER BY LastName';
                $statement = self::$connection->prepare($sql);
                $statement->execute();
                $result = $statement->fetchAll();
                self::$message = "Alle rijen van Person zijn ingelezen.";
            } catch (\PDOException $exception) {
                self::$message = $exception->getMessage();
                self::$message = "De tabel Person is leeg.";
            }
        } 
        return $result;
    }
    public static function Update($post) {
        $success = 0;
        if (self::connect()) {
            $updatePerson= array(
                'Id'=> \ModernWays\Helpers::escape($post['Id']),
                'FirstName'=> \ModernWays\Helpers::escape($post['FirstName']),
                'LastName'=> \ModernWays\Helpers::escape($post['LastName']),
                'Email'=> \ModernWays\Helpers::escape($post['Email']),
                'Address1'=> \ModernWays\Helpers::escape($post['Address1']),
                'Address2'=> \ModernWays\Helpers::escape($post['Address2']),
                'PostalCode'=> \ModernWays\Helpers::escape($post['PostalCode']),
                'City'=> \ModernWays\Helpers::escape($post['City']),
                'CountryId'=> \ModernWays\Helpers::escape($post['CountryId']),
                'Phone1'=> \ModernWays\Helpers::escape($post['Phone1']),
                'Birthday'=> \ModernWays\Helpers::escape($post['Birthday']),
                'Rating'=> \ModernWays\Helpers::escape($post['Rating'])
                );

            try {
                $sql= 'UPDATE Person SET 
                    FirstName = :FirstName,
                    LastName = :LastName,
                    Email = :Email,
                    Address1 = :Address1,
                    Address2 = :Address2,
                    PostalCode = :PostalCode,
                    City = :City,
                    CountryId = :CountryId,
                    Phone1 = :Phone1,
                    Birthday = :Birthday,
                    Rating = :Rating,
                    WHERE Id = :Id';
                $statement= self::$connection->prepare($sql);
                //$statement -> bindParam(':Id', $updatePerson['Id']);
                $statement -> bindParam(':FirstName', $updatePerson['FirstName']);
                $statement -> bindParam(':LastName', $updatePerson['LastName']);
                $statement -> bindParam(':Email', $updatePerson['Email']);
                $statement -> bindParam(':Address1', $updatePerson['Address1']);
                $statement -> bindParam(':Address2', $updatePerson['Address2']);
                $statement -> bindParam(':PostalCode', $updatePerson['PostalCode']);
                $statement -> bindParam(':City', $updatePerson['City']);
                $statement -> bindParam(':CountryId', $updatePerson['CountryId']);
                $statement -> bindParam(':Phone1', $updatePerson['Phone1']);
                $statement -> bindParam(':Birthday', $updatePerson['Birthday']);
                $statement -> bindParam(':Rating', $updatePerson['Rating']);
                $statement->execute($updatePerson);
                $success = $statement->rowCount();
                if ($success == 0) {
                self::$message = "De persoon met de naam {$updatePerson['FirstName']} {$updatePerson['LastName']} is niet gevonden.";
                } else {
                self::$message = "De persoon met de naam {$updatePerson['FirstName']} {$updatePerson['LastName']} is geüpdated.";
                }
            } catch (\PDOException $exception) {
                self::$message = "De persoon met de naam {$updatePerson['FirstName']} {$updatePerson['LastName']} is niet geüpdated. Syntax error: { $exception->getMessage()}";
            }
        }    
        return $success;
    }
}