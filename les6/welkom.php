<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" type="text/css" href="register.css">

    <title>Welkom</title>
</head>
<body>
    <h1>Welkom op onze website!</h1>
    <article>
        <ol>
            <fieldset>
            <legend>Account gegevens</legend>
            
            <li>Voornaam:
                <?php echo isset($_POST['firstname']) ? $_POST['firstname'] : 'firstname niet opgegeven!'; ?>
            </li>
            <li>Familienaam:
                <?php echo isset($_POST['lastname']) ? $_POST['lastname'] : 'lastname niet opgegeven!'; ?>
            </li>
            <li>Email:
                <?php echo isset($_POST['email']) ? $_POST['email'] : 'email niet opgegeven!'; ?>
            </li>
            <li>Password:
                <?php echo isset($_POST['password']) ? $_POST['password'] : 'password niet opgegeven!'; ?>
            </li>
            </fieldset>
            
        </ol>
        <ol>
            <fieldset>
            <legend>Adres gegevens</legend>
            
            <li>Adres 1:
                <?php echo isset($_POST['Adres1']) ? $_POST['Adres1'] : 'Adres 1 niet opgegeven!'; ?>
            </li>
            <li>Adres 2:
                <?php echo isset($_POST['Adres2']) ? $_POST['Adres2'] : 'Adres 2 niet opgegeven!'; ?>
            </li>
            <li>Stad:
                <?php echo isset($_POST['Stad']) ? $_POST['Stad'] : 'Stad niet opgegeven!'; ?>
            </li>
            <li>Postcode:
                <?php echo isset($_POST['Postcode']) ? $_POST['Postcode'] : 'Postcode niet opgegeven!'; ?>
            </li>
            <li>Land:
                <?php echo isset($_POST['Land']) ? $_POST['Land'] : 'Land niet opgegeven!'; ?>
            </li>
            </fieldset>
            
        </ol>
        <ol>
            <fieldset>
            <legend>Persoonlijke gegevens</legend>
            
            <li>Gsm:
                <?php echo isset($_POST['Gsm']) ? $_POST['Gsm'] : 'Gsm niet opgegeven!'; ?>
            </li>
            <li>Geboortedatum:
                <?php echo isset($_POST['Geboortedatum']) ? $_POST['Geboortedatum'] : 'Geboortedatum niet opgegeven!'; ?>
            </li>
            <li>Tevredenheid:
                <?php echo isset($_POST['Tevredenheid']) ? $_POST['Tevredenheid'] : 'Tevredenheid niet opgegeven!'; ?>
            </li>
            <li>geslacht:
                <?php echo isset($_POST['geslacht']) ? $_POST['geslacht'] : 'geslacht niet opgegeven!'; ?>
            </li>
            <li>Afdeling:
                <?php echo isset($_POST['Afdeling']) ? $_POST['Afdeling'] : 'Afdeling niet opgegeven!'; ?>
            </li>
            <li>Module:
                <?php echo isset($_POST['Module']) ? $_POST['Module'] : 'Module niet opgegeven!'; ?>
            </li>
            </fieldset>
            
        </ol>
    </article>
    
    
</body>
</html>

