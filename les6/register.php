<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" type="text/css" href="register.css">
    <title>Document</title>
</head>
<body>
  
   <!-- <form method="post" action="welkom.php"> -->
   <!-- https://www.w3schools.com/php/php_form_validation.asp -->
    <form method="post" action="welkom.php">

      <h1>Vul het registratieformulier in.</h1>
      <h1>Velden met een * zijn verplicht</h1>

      <fieldset>
        
        <legend>Account gegevens</legend>
  <div id="input">
    <label for="firstname">Voornaam <span>*</span></label>
    <input type="text" name="firstname" id="firstname" required="required" />
</div>
  <div id="input">
    <label for="lastname">Familienaam <span>*</span></label>
    <input type="text" name="lastname" id="lastname" required="required" />
</div>
  <div id="input">
    <label for="email">Email <span>*</span></label>
    <input type="email" name="email" id="email" required="required" placeholder="user@provider.name" />
</div>
  <div id="input">
    <label for="password">Wachtwoord <span>*</span></label>
    <input type="password" name="password" maxlength="12" id="password" required="required" />
</div>
<div id="input">
    <label for="confirm_password">Bevestig wachtwoord <span>*</span></label>
    <input type="password" name="confirm_password" id="confirm_password" required="required" />
</div>

<!-- Javascript voor confirmpassword-->
<script>
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<!-- -->
  </fieldset>
  <fieldset>
    <legend>Adres gegevens</legend>
    <!--(div>(label+span+input:text))*4  -->
    <div id="input">
      <label for="adres1">Adres 1 <span>*</span></label>
      
      <input type="text" name="adres1" id="adres1" required="required" >
    </div>
    <div id="input">
      <label for="adres2">Adres 2</label>
     <span> </span>
      <input type="text" name="adres2" id="adres2" >
    </div>
    <div id="input">
      <label for="stad">Stad <span>*</span></label>
      
      <input type="text" name="stad" id="stad" required="required">
    </div>
    <div id="input">
      <label for="postcode">Postcode <span>*</span></label>
     
      <input type="text" name="postcode" id="postcode" required="required">
    </div> 
    <!--div>(label+select>option*4) -->
    <div id="input"><label for="land">Kies een Land</label><select name="land" id="land">
        <option value="België">België</option>
        <option value="France">France</option>
        <option value="Nederland">Nederland</option>
        <option value="England">England</option>
      </select></div>
  </fieldset>
  <fieldset>
    <legend>Persoonlijke gegevens</legend>
    <div id="input">
      <label for="gsm">GSM</label>
      <input type="text" name="gsm" id="gsm" >
    </div>
  <div id="input">
      <label for="geboortedatum">Geboortedatum </label>
      <input type="Date" name="geboortedatum" id="geboortedatum" >
    </div>
        <div id="input">
      <label for="tevredenheid">Hoe tevreden ben je? </label>
      <input type="range" min="-4" max="4" step="1" value="0" name="tevredenheid" id="tevredenheid" >
    </div id="input">
  <!--div>(input:radio+label)*3-->
  <div id="input">
    <label for="geslacht">Kies jouw geslacht</label>
    </div>
    <div >
      <div id="geslacht">
      <label for="Man" >Man</label><input type="radio" name="geslacht" value="Man" id="geslacht">
      </div>
      <div id="geslacht">
      <label for="Vrouw" >Vrouw</label><input type="radio" name="geslacht" value="Vrouw "id="geslacht">
      </div>
      <div id="geslacht">
      <label for="Onbekend" >Onbekend</label><input type="radio" name="geslacht" value="Onbekend" id="geslacht">
      </div>
  </div>
  <!--div>select>option*3-->
  <div>
    <label for="afdeling">Kies een afdeling</label>
      <select name="afdeling" id="afdeling">
          <option value="HBO Informatica">HBO Informatica</option>
          <option value="HBO Boekhouden">HBO Boekhouden</option>
      </select>
    </div>
    <div>
    <label for="module">Kies een module</label>
      <select name="module" id="module">
          <option value="programmeren 3">programmeren 3</option>
          <option value="programmeren 4">programmeren 4</option>
          <option value="programmeren 5">programmeren 5</option>
      </select>
    </div>
   
    </fieldset>
    <fieldset>
        <legend>Versturen</legend>
  <div id="Knop">
    <button  type="submit" name"uc" value="User-Insert">Verzenden</button>
  </div>
   </fieldset>
</form> 

</body>
</html>